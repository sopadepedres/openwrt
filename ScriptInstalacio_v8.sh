IPServidorRadius="185.242.249.85"
IPServidorRadius2="185.242.249.144"
IPServidorGrafana="IPServidorGrafana"
PassRadius="contrasenya"
INET="eth0.1"
Hostname="NomdelRouter"

#Cal canviar la contrasenya
echo "Has de canviar la contrasenya del router:"
passwd

#Si vols posar la teva clau pública dins el router
#ssh root@192.168.1.1 "tee -a /etc/dropbear/authorized_keys" < ~/.ssh/id_rsa.pub

#Comencem confiurant la interficie LAN que sera la wireless
uci set network.lan=interface
uci delete network.lan.type
uci set network.lan.ifname='wlan0'
uci set network.lan.proto='static'
uci set network.lan.ipaddr='192.168.168.1'
uci set network.lan.netmask='255.255.255.0'

#Configurem la interficie WAN que sera la ethernet
uci set network.WAN=interface
uci set network.WAN.proto='dhcp'
uci set network.WAN.ifname=$INET

#Configurem el wireless
uci set wireless.default_radio0=wifi-iface
uci set wireless.default_radio0.device='radio0'
uci set wireless.default_radio0.network='lan'
uci set wireless.default_radio0.mode='ap'
uci set wireless.default_radio0.ssid='CapEstudiantSenseInternet'
uci set wireless.default_radio0.encryption='none'
uci delete wireless.radio0.disabled
uci set wireeless.default_radio0.isolate='1'

#Guardem la configuracio
uci commit
reload_config

#Ens esperem fins que el dhcp hagi donat ip al router
while true; do ping -c1 gnu.org > /dev/null && break; done

#Actualitzem els paquets de la openwrt
opkg update
#opkg install `opkg list-upgradable`

#Instalem els paquets necessaris per hotspot i el tor
opkg install nodogsplash freeradius3-utils tor
#Cal canviar la conf de nodogsplash i canviar br-lan per wlan0
ln -s /usr/lib/freeradius3/libfreeradius-radius.so /usr/lib/

#Primera configuracio de tor, separem entre espais perque tingui temps de crear directoris i configurarse
echo "HiddenServiceDir /var/lib/tor/hidden_service/" >> /etc/tor/torrc
echo "HiddenServicePort 22 127.0.0.1:22" >> /etc/tor/torrc
/etc/init.d/tor restart

#Instalem paquets necessaris per baixar fitxers https
opkg install libustream-openssl20150806 ca-certificates

#Instalem els fitxers configurats per nodogsplash de gitlab
wget https://gitlab.com/sopadepedres/openwrt/-/raw/master/nodogsplash/binauth_sitewide.sh -O /etc/nodogsplash/binauth_sitewide.sh
wget https://gitlab.com/sopadepedres/openwrt/-/raw/master/nodogsplash/htdocs/splash.css -O /etc/nodogsplash/htdocs/splash.css
wget https://gitlab.com/sopadepedres/openwrt/-/raw/master/nodogsplash/htdocs/splash_sitewide.html -O /etc/nodogsplash/htdocs/splash_sitewide.html
wget https://gitlab.com/sopadepedres/openwrt/-/raw/master/nodogsplash/htdocs/splash.html -O /etc/nodogsplash/htdocs/splash.html
wget https://gitlab.com/sopadepedres/openwrt/-/raw/master/nodogsplash/htdocs/images/splash.jpg -O /etc/nodogsplash/htdocs/images/splash.jpg
wget https://gitlab.com/sopadepedres/openwrt/-/raw/master/nodogsplash/htdocs/images/banner_estudiants.png -O /etc/nodogsplash/htdocs/images/banner_estudiants.png

sed -e "s/IP_Server_Radius:1812 auth secret/$IPServidorRadius:1812 auth $PassRadius/g" /etc/nodogsplash/binauth_sitewide.sh > /tmp/binauth_sitewide.sh
sed -e "s/IP_Server_Radius2:1812 auth secret/$IPServidorRadius2:1812 auth $PassRadius/g" /tmp/binauth_sitewide.sh > /etc/nodogsplash/binauth_sitewide.sh 
chmod u+x /etc/nodogsplash/binauth_sitewide.sh

uci set nodogsplash.@nodogsplash[0]=nodogsplash
uci set nodogsplash.@nodogsplash[0].enabled='1'
uci set nodogsplash.@nodogsplash[0].fwhook_enabled='1'
uci set nodogsplash.@nodogsplash[0].gatewayinterface='wlan0'
uci set nodogsplash.@nodogsplash[0].gatewayname='CapEstudiantSenseInternet'
uci set nodogsplash.@nodogsplash[0].maxclients='250'
uci set nodogsplash.@nodogsplash[0].preauthidletimeout='30'
uci set nodogsplash.@nodogsplash[0].authidletimeout='120'
uci set nodogsplash.@nodogsplash[0].sessiontimeout='1200'
uci set nodogsplash.@nodogsplash[0].checkinterval='600'
uci set nodogsplash.@nodogsplash[0].splashpage='splash_sitewide.html'
uci set nodogsplash.@nodogsplash[0].binauth='/etc/nodogsplash/binauth_sitewide.sh'
uci del_list nodogsplash.@nodogsplash[0].authenticated_users='allow all' 
uci add_list nodogsplash.@nodogsplash[0].authenticated_users='block to 192.168.0.0/16'
uci add_list nodogsplash.@nodogsplash[0].authenticated_users='block to 172.16.0.0/16'
uci add_list nodogsplash.@nodogsplash[0].authenticated_users='block to 10.0.0.0/8'
uci add_list nodogsplash.@nodogsplash[0].authenticated_users='allow all'
uci commit
reload_config

/etc/init.d/nodogsplash restart

uci set firewall.nodogsplash=include
uci set firewall.nodogsplash.type='script'
uci set firewall.nodogsplash.path='/usr/lib/nodogsplash/restart.sh'
uci set firewall.@zone[1].network='wan wan6 WAN'
uci set system.@system[0].hostname="$Hostname"
uci commit
reload_config

/etc/init.d/firewall restart

#Segona configuracio de tor per deixar-la estable quan rebotem el router
sed -e "s/HiddenServiceDir \/var\/lib\/tor/HiddenServiceDir \/etc\/tor/g" /etc/tor/torrc > /tmp/torrc
mv /tmp/torrc /etc/tor/
mv /var/lib/tor/hidden_service /etc/tor/
/etc/init.d/tor restart

#Comunicacio amb Grafana+Influxdb amb collectd
opkg install luci-app-statistics collectd collectd-mod-cpu collectd-mod-interface collectd-mod-iwinfo collectd-mod-load collectd-mod-memory collectd-mod-network collectd-mod-uptime

/etc/init.d/luci_statistics enable
/etc/init.d/collectd enable

uci set luci_statistics.collectd.Hostname="$Hostname"
uci set luci_statistics.collectd_network.enable='1'
uci set luci_statistics.collectd_network.Forward='0'
uci set luci_statistics.collectd_interface.Interfaces="$INET wlan0"
uci set luci_statistics.collectd_uptime.enable='1'

uci add luci_statistics collectd_network_server
uci set luci_statistics.@collectd_network_server[0].port='25826'
uci set luci_statistics.@collectd_network_server[0].host="$IPServidorGrafana"
uci commit 
reload_config

/etc/init.d/collectd restart 

reboot
#Falta configurar el sqm

# ==============================
# opkg install luci-app-sqm  # install the SQM modules to get fq_codel etc
# Set Smart Queue Management (SQM) values for your own network
#
# Use a speed test (http://dslreports.com/speedtest) to determine 
# the speed of your own network, then set the speeds  accordingly.
# Speeds below are in kbits per second (3000 = 3 megabits/sec)
# For details about setting the SQM for your router, see:
# http://wiki.openwrt.org/doc/howto/sqm
# Set DOWNLOADSPEED, UPLOADSPEED, WANIF and then uncomment 18 lines
#
# DOWNLOADSPEED=20000
# UPLOADSPEED=2000
# WANIF=eth0
# echo 'Setting SQM on '$WANIF ' to ' $DOWNLOADSPEED/$UPLOADSPEED 'kbps down/up'
# uci set sqm.@queue[0].interface=$WANIF
# uci set sqm.@queue[0].enabled=1
# uci set sqm.@queue[0].download=$DOWNLOADSPEED
# uci set sqm.@queue[0].upload=$UPLOADSPEED
# uci set sqm.@queue[0].script='simple.qos' # Already the default
# uci set sqm.@queue[0].qdisc='fq_codel'
# uci set sqm.@queue[0].itarget='auto'
# uci set sqm.@queue[0].etarget='auto'
# uci set sqm.@queue[0].linklayer='atm'
# uci set sqm.@queue[0].overhead='44'
# uci commit sqm
# /etc/init.d/sqm restart
# /etc/init.d/sqm enable
