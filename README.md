# OpenWRT a Cap Estudiant Sense Internet

Aquí detallarem totes les instruccions de com muntar nodes openwrt amb portal captiu perquè s'hi puguin connectar tots aquells estudiants sense internet durant els confinaments.

## Passos a seguir

* Instalar l'openwrt (Depèn del router) a partir de les instruccions de http://openwrt.org
* Procurar instal·lar la versió 19.07.2
* Configurar la xarxa
   - Habilitar la xarxa Wireless amb CapEstudiantSenseInternet
   - Configurar la ínterficie Wireless estigui a la xarxa LAN, i li poso la ip 192.168.168.1 i posem un servidor dhcp només per la interfície wifi
   - La xarxa eth0 ha d'estar a la xarxa WAN, agafa la ip per dhcp del router casolà
   - Hem afegit aquestes dues línies d'iptables a /etc/firewall.user per poder accedir a l'antena des de la xarxa de dins des de la IP 192.168.0.5, en aquest cas.
--- 
       iptables -A INPUT -i eth0 -p tcp -s 192.168.0.5 --dport 22  -j ACCEPT
       iptables -A OUTPUT -o eth0 -p tcp -d 192.168.0.5 --sport 22  -j ACCEPT
---
* Actualitzar la openwrt:
---
    opkg update
    opkg install `opkg list-upgradable`
---
* Instalar el freeradius3-utils
---
    opkg update
    opkg freeradius3-utils
---
* Instalar el nodogsplash (portal captiu)
---
    opkg install nodogsplash
---
* Posar com configuracio del /etc/config/nodogsplash
---
    config nodogsplash
        option enabled '1'
        option fwhook_enabled '1'
        option gatewayinterface 'wlan0'
        option gatewayname 'CapEstudiantSenseInternet'
        option maxclients '250'                       
        option preauthidletimeout '30'
        option authidletimeout '120'  
        option sessiontimeout '1200'
        option checkinterval '600'  
        list authenticated_users 'allow all'
        list users_to_router 'allow tcp port 22'
        list users_to_router 'allow tcp port 23'
        list users_to_router 'allow tcp port 53'
        list users_to_router 'allow udp port 53'
        list users_to_router 'allow udp port 67'
        list users_to_router 'allow tcp port 80'
        list users_to_router 'allow tcp port 443'
        #option fasport 1812                     
        #option fasremote ip IP_Servidor_Radius
        option splashpage 'splash_sitewide.html'
        option binauth '/etc/nodogsplash/binauth_sitewide.sh'
---
* Cal canviar la configuració del nodogsplash
* Afegir la imatge del logo de Mataró Sensefils (splash.jpg) i el logo de la campanya (banner_estudiants.png) al directori /etc/nodogsplash/htdocs/images/
* Canviar el fitxer /etc/nodogsplash/htdocs/splash_sitewide.html per maquetar la pàgina de presentació, afegeixo el fitxer canviat a l'arrel.
* Canviar el fitxer /etc/nodogsplash/htdocs/splash.css per maquetar els estils, afegim el fitxer canviat a l'arrel.
*  Canviar el següent del fitxer /etc/nodogsplash/binauth_sitewide.sh
   - Afegir la línia:
---
     echo "User-name=$USERNAME,User-Password=$PASSWORD" | /usr/bin/radclient IP_Servidor_Radius:1812 auth secret 
        # Aqui passem usuari i contrasenya escrit a la web per enviar al servidor radius amb ip:port auth password
        res=$? 
        if [ $res -eq 0 ]; then 
---

- Canviar per la línia que hi ha a l'original següent perquè es validi al servidor radius 
---
      if [ "$USERNAME" = "Staff" -a "$PASSWORD" = "weneedit" ]; then
---
* Canviar permisos de binauth_sitewide.sh perquè es pugui executar
  chmod u+x binauth_sitewide.sh

* Falla que no troba libfreeradius-radius.so, cal afegir un enllaç
---
    cd /usr/lib
    ln -s freeradius3/libfreeradius-radius.so .
---
* Per aïllar els clients wifi entre ells cal afegir la següent comanda del fitxer /etc/config/wireless a on diu config wifi-iface
---
    option isolate 1

TODO
* Mirar com configurar el https amb certificat, però es necessita servidor extern i instal·lar programari que ocupa bastant a l'antena.
* Mirar com configurar de forma automàtica sense Luci, amb un script tota la configuració.
* Limitar ample de banda